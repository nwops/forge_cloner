# Forge Cloner
A script to automate cloning and syncing of puppet forge modules and git repos.  

## How it works
1. Reads the puppetfile entries
2. Gets the project source url of the module from the puppet forge unless git url is provided
3. Clones the git url
4. Creates a repo in the VCS (Github, Github Enterprise)
5. Mirrors the contents of the git repo to the chosen VCS namespace

## Requirements
1. A Github token
2. Access to create modules in the VCS namespace
3. Ruby 2.5+
4. Bundler 2.0+
5. [GITHUB_TOKEN](https://help.github.com/en/github/authenticating-to-github/creating-a-personal-access-token) environment variables set
6. Optional proxy in git set
7. Optional SSL_CERT_FILE set if using a proxy or custom CA

```shell
export GITHUB_TOKEN='sdafdsfasdfasdfasdfasdfasdf'
export SSL_CERT_FILE='path_to_custom_root_ca.cer'
```

## Usage
Once you have the desired Puppetfile run the following command

`ruby forge_cloner.rb Puppetfile`

**NOTE** The name of the Puppetfile is arbitrary. You could have different files for different namespaces. 

### Puppetfile
This script tries to adhere to the same rules that apply to the Puppetfile format.  While the action taken on this file is much different than r10k or librarian-puppet, the rules are almost identical.

Use `github` to define the settings the script will use when cloning if using github. 

The following settings can be used:
* namespace (what namespace/group you want modules to be created)
* token (Please use `ENV['GITHUB_TOKEN']`)
* proxy (optional)
* api_url (optional)
* modules_dir (optional)

Example: `github 'https://github.com', token: ENV['GITHUB_TOKEN'], namespace: 'nwops'`

Use `mod` to define a module or git repo module.
* `mod 'puppetlabs/mount_core'`
* `mod 'puppetlabs/peadm', git: 'https://github.com/puppetlabs/puppetlabs-peadm'`


## TODO
* Create some CI scripts for jenkins, Gitlab to automate running this script on commits. 
* Add Gitlab support (should be easy, since gitlab creates repos not already created)
* Parallelize and queue 
